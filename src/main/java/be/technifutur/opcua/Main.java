package be.technifutur.opcua;

import org.eclipse.milo.opcua.sdk.client.OpcUaClient;
import org.eclipse.milo.opcua.sdk.client.api.UaClient;
import org.eclipse.milo.opcua.sdk.client.api.config.OpcUaClientConfig;
import org.eclipse.milo.opcua.stack.core.UaException;
import org.eclipse.milo.opcua.stack.core.serialization.UaRequestMessage;
import org.eclipse.milo.opcua.stack.core.types.builtin.ByteString;
import org.eclipse.milo.opcua.stack.core.types.builtin.ExpandedNodeId;
import org.eclipse.milo.opcua.stack.core.types.builtin.LocalizedText;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UByte;
import org.eclipse.milo.opcua.stack.core.types.enumerated.ApplicationType;
import org.eclipse.milo.opcua.stack.core.types.enumerated.MessageSecurityMode;
import org.eclipse.milo.opcua.stack.core.types.enumerated.UserTokenType;
import org.eclipse.milo.opcua.stack.core.types.structured.ApplicationDescription;
import org.eclipse.milo.opcua.stack.core.types.structured.EndpointDescription;
import org.eclipse.milo.opcua.stack.core.types.structured.UserTokenPolicy;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world!");

        // Configuration du client OPC UA
        EndpointDescription endpoint = getEndpointDescription();
        OpcUaClientConfig config = OpcUaClientConfig.builder()
                .setEndpoint(endpoint)
                .setApplicationName(LocalizedText.english("Java OPC UA Client"))
                .build();
        // Création du client OPC UA
        OpcUaClient client;

        {
            try {
                client = OpcUaClient.create(config);
                printClient(client);
                // Connexion au serveur OPC UA
                CompletableFuture<UaClient> connect = client.connect();
                System.out.println("connecté");
                System.out.println(connect);
                System.out.println(connect.state());

                CompletableFuture<OpcUaClient> future = new CompletableFuture<>();
                try {
                    new BrowseExample().run(client, future);
                    future.whenCompleteAsync((a, b) ->
                    {
                        OpcUaClient a1 = a;
                        System.out.println(a1.getNamespaceTable());
                        System.out.println(b);
                    });
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                //UaClient uaClient = connect.get();
                //System.out.println(uaClient.getConfig());


                // Interagir avec le serveur OPC UA...
                //System.out.println(client);

                // Déconnexion du serveur OPC UA
                client.disconnect().get();

            } catch (UaException e) {
                throw new RuntimeException(e);
            } catch (ExecutionException e) {
                throw new RuntimeException(e);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void printClient(OpcUaClient client) {
        OpcUaClientConfig config = client.getConfig();
        System.out.println(config.getApplicationName());
        System.out.println(config.getApplicationUri());
        System.out.println(config.getIdentityProvider());
    }


    private static EndpointDescription getEndpointDescription() {
        String endpointUrl = "opc.tcp://10.45.22.200:4840/"; // Remplacez par l'adresse du serveur OPC UA
        ApplicationDescription server = getApplicationDescription();
        ByteString serverCertificate = null; // SecurityPolicy (peut être null pour utiliser la valeur par défaut)
        MessageSecurityMode securityMode = MessageSecurityMode.None; // SecurityMode (peut être null pour utiliser la valeur par défaut)
        String securityPolicyUri = getsecurityPolicyUri();// ServerCertificate (peut être null pour utiliser la valeur par défaut)
        UserTokenPolicy[] userIdentityTokens = new UserTokenPolicy[]{new UserTokenPolicy("policyId", UserTokenType.Anonymous, "issuedTokenType", "issuerEndpointUrl", "securityPolicyUri")};
        // profil de transport TCP pour la communication et le protocole binaire OPC UA pour l'échange de données.
        String transportProfileUri = "http://opcfoundation.org/UA-Profile/Transport/uatcp-uasc-uabinary";
        UByte securityLevel = UByte.valueOf(0);

        EndpointDescription endpoint = new EndpointDescription(
                endpointUrl,
                server,
                serverCertificate,
                securityMode,
                securityPolicyUri, // ServerCertificate (peut être null pour utiliser la valeur par défaut)
                userIdentityTokens, // ApplicationUri (peut être null pour utiliser la valeur par défaut)
                transportProfileUri,  // TransportProfileUri (peut être null pour utiliser la valeur par défaut)
                securityLevel
        ) {
            @Override
            public ExpandedNodeId getTypeId() {
                System.out.println("Main.getTypeId");
                return super.getTypeId();
            }

            @Override
            public ExpandedNodeId getXmlEncodingId() {
                System.out.println("Main.getXmlEncodingId");
                return super.getXmlEncodingId();
            }

            @Override
            public ExpandedNodeId getBinaryEncodingId() {
                System.out.println("Main.getBinaryEncodingId");
                return super.getBinaryEncodingId();
            }

            @Override
            public String getEndpointUrl() {
                String url = super.getEndpointUrl();
                System.out.printf("Main.getEndpointUrl: %s%n", url);
                return url;
            }

            @Override
            public ApplicationDescription getServer() {
                ApplicationDescription serveur = super.getServer();
                System.out.printf("Main.getServer: %s%n", serveur);
                return serveur;
            }

            @Override
            public ByteString getServerCertificate() {
                System.out.println("Main.getServerCertificate");
                return super.getServerCertificate();
            }

            @Override
            public MessageSecurityMode getSecurityMode() {
                MessageSecurityMode mode = super.getSecurityMode();
                System.out.printf("Main.getSecurityMode: %s%n", mode);
                return mode;
            }

            @Override
            public String getSecurityPolicyUri() {
                String policyUri = super.getSecurityPolicyUri();
                System.out.printf("Main.getSecurityPolicyUri: %s%n", policyUri);
                return policyUri;
            }

            @Override
            public UserTokenPolicy[] getUserIdentityTokens() {
                UserTokenPolicy[] tokens = super.getUserIdentityTokens();
                System.out.printf("Main.getUserIdentityTokens: %s%n", Arrays.toString(tokens));
                return tokens;
            }

            @Override
            public String getTransportProfileUri() {
                System.out.println("Main.getTransportProfileUri");
                return super.getTransportProfileUri();
            }

            @Override
            public UByte getSecurityLevel() {
                System.out.println("Main.getSecurityLevel");
                return super.getSecurityLevel();
            }
        };
        return endpoint;
    }

    private static ApplicationDescription getApplicationDescription() {
        String applicationUri = "https://www.siemens.com/s7-1500";
        String productUri = "productUri";
        LocalizedText applicationName = LocalizedText.english("SIMANTEC");
        ApplicationType applicationType = ApplicationType.Client;
        String gatewayServerUri = "gatewayServerUri";
        String discoveryProfileUri = "discoveryProfileUri";
        String[] discoveryUrls = new String[0];
        return new ApplicationDescription(
                applicationUri,
                productUri,
                applicationName,
                applicationType,
                gatewayServerUri,
                discoveryProfileUri,
                discoveryUrls);
    }

    private static String getsecurityPolicyUri() {
        /*
        La valeur à utiliser pour securityPolicyUri dépend de la configuration de sécurité du serveur OPC UA
        auquel vous vous connectez. Les valeurs courantes pour securityPolicyUri incluent :

        "http://opcfoundation.org/UA/SecurityPolicy#None" : Aucune politique de sécurité n'est appliquée.
        "http://opcfoundation.org/UA/SecurityPolicy#Basic128Rsa15" : Politique de sécurité de base utilisant le chiffrement RSA avec une taille de clé de 128 bits.
        "http://opcfoundation.org/UA/SecurityPolicy#Basic256" : Politique de sécurité de base utilisant le chiffrement AES avec une taille de clé de 256 bits.
        "http://opcfoundation.org/UA/SecurityPolicy#Basic256Sha256" : Politique de sécurité de base utilisant le chiffrement AES avec une taille de clé de 256 bits et le hachage SHA-256.
         */
        return "http://opcfoundation.org/UA/SecurityPolicy#None";
    }
}

